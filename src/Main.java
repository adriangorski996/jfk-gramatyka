import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.Scanner;

import static java.lang.System.out;

public class Main {

    public static void main(String[] args) throws Exception {
        DateTimeUtils.fillMap();
        String input;
        out.println("Key in the input string:");
        try (Scanner reader = new Scanner(System.in)) {
            input = reader.nextLine();
        }
        out.println();

        CharStream charStream = CharStreams.fromString(input);
        DateGrammarLexer lexer = new DateGrammarLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        DateGrammarParser parser = new DateGrammarParser(tokens);
        ParseTree tree = parser.expression();

        parser.setBuildParseTree(true);
        int errors = parser.getNumberOfSyntaxErrors();

        out.println("Number of syntax errors: " + errors);
        out.println(tree.toStringTree(parser));



        if (0 == errors) {
            DateGrammarVisitorImpl visitor= new DateGrammarVisitorImpl();
            out.println("Result = " +  visitor.visit(tree));
        }

    }
}
