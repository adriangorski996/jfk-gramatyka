// Generated from C:/Users/kryst/IdeaProjects/date-grammar-antlr/src\DateGrammar.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link DateGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface DateGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link DateGrammarParser#date}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDate(DateGrammarParser.DateContext ctx);
	/**
	 * Visit a parse tree produced by {@link DateGrammarParser#dateTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDateTime(DateGrammarParser.DateTimeContext ctx);
	/**
	 * Visit a parse tree produced by {@link DateGrammarParser#timeSpan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeSpan(DateGrammarParser.TimeSpanContext ctx);
	/**
	 * Visit a parse tree produced by {@link DateGrammarParser#operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperation(DateGrammarParser.OperationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DateGrammarParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary(DateGrammarParser.UnaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link DateGrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(DateGrammarParser.ExpressionContext ctx);
}