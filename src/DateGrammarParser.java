// Generated from C:/Users/kryst/IdeaProjects/date-grammar-antlr/src\DateGrammar.g4 by ANTLR 4.7
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DateGrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, WhiteSpace=3, TIMESPAN=4, T=5, Y=6, M=7, DY=8, H=9, MIN=10, 
		S=11, DATETIME=12, DATE=13, TIME=14, HOUR=15, MINUTE=16, SECOND=17, YEAR=18, 
		MONTH=19, LONGMONTH=20, SHORTMONTH=21, DAY=22, JAN=23, FEB=24, MAR=25, 
		APR=26, MAY=27, JUN=28, JUL=29, AUG=30, SEP=31, OCT=32, NOV=33, DEC=34, 
		LONGMONTHDAY=35, SHORTMONTHDAY=36, SHORTFEBDAY=37, LONGFEBDAY=38, SEPARATOR=39, 
		CH=40, SINCE=41, PLUS=42, MINUS=43;
	public static final int
		RULE_date = 0, RULE_dateTime = 1, RULE_timeSpan = 2, RULE_operation = 3, 
		RULE_unary = 4, RULE_expression = 5;
	public static final String[] ruleNames = {
		"date", "dateTime", "timeSpan", "operation", "unary", "expression"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'", null, null, null, "'Y'", "'M'", "'D'", "'h'", "'m'", 
		"'s'", null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "'*'", "'&'", "'+'", "'|'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, "WhiteSpace", "TIMESPAN", "T", "Y", "M", "DY", "H", 
		"MIN", "S", "DATETIME", "DATE", "TIME", "HOUR", "MINUTE", "SECOND", "YEAR", 
		"MONTH", "LONGMONTH", "SHORTMONTH", "DAY", "JAN", "FEB", "MAR", "APR", 
		"MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "LONGMONTHDAY", 
		"SHORTMONTHDAY", "SHORTFEBDAY", "LONGFEBDAY", "SEPARATOR", "CH", "SINCE", 
		"PLUS", "MINUS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "DateGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public DateGrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class DateContext extends ParserRuleContext {
		public TerminalNode DATE() { return getToken(DateGrammarParser.DATE, 0); }
		public DateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_date; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).enterDate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).exitDate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DateGrammarVisitor ) return ((DateGrammarVisitor<? extends T>)visitor).visitDate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateContext date() throws RecognitionException {
		DateContext _localctx = new DateContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_date);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(12);
			match(DATE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateTimeContext extends ParserRuleContext {
		public TerminalNode DATETIME() { return getToken(DateGrammarParser.DATETIME, 0); }
		public DateTimeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dateTime; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).enterDateTime(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).exitDateTime(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DateGrammarVisitor ) return ((DateGrammarVisitor<? extends T>)visitor).visitDateTime(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateTimeContext dateTime() throws RecognitionException {
		DateTimeContext _localctx = new DateTimeContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_dateTime);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(14);
			match(DATETIME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeSpanContext extends ParserRuleContext {
		public TerminalNode TIMESPAN() { return getToken(DateGrammarParser.TIMESPAN, 0); }
		public TimeSpanContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeSpan; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).enterTimeSpan(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).exitTimeSpan(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DateGrammarVisitor ) return ((DateGrammarVisitor<? extends T>)visitor).visitTimeSpan(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TimeSpanContext timeSpan() throws RecognitionException {
		TimeSpanContext _localctx = new TimeSpanContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_timeSpan);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(16);
			match(TIMESPAN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperationContext extends ParserRuleContext {
		public Token op;
		public TerminalNode PLUS() { return getToken(DateGrammarParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(DateGrammarParser.MINUS, 0); }
		public List<DateContext> date() {
			return getRuleContexts(DateContext.class);
		}
		public DateContext date(int i) {
			return getRuleContext(DateContext.class,i);
		}
		public List<DateTimeContext> dateTime() {
			return getRuleContexts(DateTimeContext.class);
		}
		public DateTimeContext dateTime(int i) {
			return getRuleContext(DateTimeContext.class,i);
		}
		public List<TimeSpanContext> timeSpan() {
			return getRuleContexts(TimeSpanContext.class);
		}
		public TimeSpanContext timeSpan(int i) {
			return getRuleContext(TimeSpanContext.class,i);
		}
		public List<UnaryContext> unary() {
			return getRuleContexts(UnaryContext.class);
		}
		public UnaryContext unary(int i) {
			return getRuleContext(UnaryContext.class,i);
		}
		public List<OperationContext> operation() {
			return getRuleContexts(OperationContext.class);
		}
		public OperationContext operation(int i) {
			return getRuleContext(OperationContext.class,i);
		}
		public OperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).enterOperation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).exitOperation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DateGrammarVisitor ) return ((DateGrammarVisitor<? extends T>)visitor).visitOperation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperationContext operation() throws RecognitionException {
		OperationContext _localctx = new OperationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_operation);
		int _la;
		try {
			setState(61);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS:
			case MINUS:
				enterOuterAlt(_localctx, 1);
				{
				setState(18);
				((OperationContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==PLUS || _la==MINUS) ) {
					((OperationContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(55);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
				case 1:
					{
					setState(24);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case DATE:
						{
						setState(19);
						date();
						}
						break;
					case DATETIME:
						{
						setState(20);
						dateTime();
						}
						break;
					case TIMESPAN:
						{
						setState(21);
						timeSpan();
						}
						break;
					case CH:
					case SINCE:
						{
						setState(22);
						unary();
						}
						break;
					case T__0:
					case PLUS:
					case MINUS:
						{
						setState(23);
						operation();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(28);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case TIMESPAN:
						{
						setState(26);
						timeSpan();
						}
						break;
					case T__0:
					case PLUS:
					case MINUS:
						{
						setState(27);
						operation();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
					break;
				case 2:
					{
					setState(32);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case TIMESPAN:
						{
						setState(30);
						timeSpan();
						}
						break;
					case T__0:
					case PLUS:
					case MINUS:
						{
						setState(31);
						operation();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(39);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case DATE:
						{
						setState(34);
						date();
						}
						break;
					case DATETIME:
						{
						setState(35);
						dateTime();
						}
						break;
					case TIMESPAN:
						{
						setState(36);
						timeSpan();
						}
						break;
					case CH:
					case SINCE:
						{
						setState(37);
						unary();
						}
						break;
					case T__0:
					case PLUS:
					case MINUS:
						{
						setState(38);
						operation();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
					break;
				case 3:
					{
					setState(46);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case DATE:
						{
						setState(41);
						date();
						}
						break;
					case DATETIME:
						{
						setState(42);
						dateTime();
						}
						break;
					case TIMESPAN:
						{
						setState(43);
						timeSpan();
						}
						break;
					case CH:
					case SINCE:
						{
						setState(44);
						unary();
						}
						break;
					case T__0:
					case PLUS:
					case MINUS:
						{
						setState(45);
						operation();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(53);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case DATE:
						{
						setState(48);
						date();
						}
						break;
					case DATETIME:
						{
						setState(49);
						dateTime();
						}
						break;
					case TIMESPAN:
						{
						setState(50);
						timeSpan();
						}
						break;
					case CH:
					case SINCE:
						{
						setState(51);
						unary();
						}
						break;
					case T__0:
					case PLUS:
					case MINUS:
						{
						setState(52);
						operation();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
					break;
				}
				}
				break;
			case T__0:
				enterOuterAlt(_localctx, 2);
				{
				setState(57);
				match(T__0);
				setState(58);
				operation();
				setState(59);
				match(T__1);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryContext extends ParserRuleContext {
		public Token u;
		public TerminalNode SINCE() { return getToken(DateGrammarParser.SINCE, 0); }
		public TerminalNode CH() { return getToken(DateGrammarParser.CH, 0); }
		public OperationContext operation() {
			return getRuleContext(OperationContext.class,0);
		}
		public DateContext date() {
			return getRuleContext(DateContext.class,0);
		}
		public DateTimeContext dateTime() {
			return getRuleContext(DateTimeContext.class,0);
		}
		public UnaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).enterUnary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).exitUnary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DateGrammarVisitor ) return ((DateGrammarVisitor<? extends T>)visitor).visitUnary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryContext unary() throws RecognitionException {
		UnaryContext _localctx = new UnaryContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_unary);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(63);
			((UnaryContext)_localctx).u = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==CH || _la==SINCE) ) {
				((UnaryContext)_localctx).u = (Token)_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(73);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(67);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case DATE:
					{
					setState(64);
					date();
					}
					break;
				case DATETIME:
					{
					setState(65);
					dateTime();
					}
					break;
				case T__0:
				case PLUS:
				case MINUS:
					{
					setState(66);
					operation();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 2:
				{
				setState(69);
				match(T__0);
				setState(70);
				operation();
				setState(71);
				match(T__1);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public OperationContext operation() {
			return getRuleContext(OperationContext.class,0);
		}
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public DateContext date() {
			return getRuleContext(DateContext.class,0);
		}
		public DateTimeContext dateTime() {
			return getRuleContext(DateTimeContext.class,0);
		}
		public TimeSpanContext timeSpan() {
			return getRuleContext(TimeSpanContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DateGrammarListener ) ((DateGrammarListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DateGrammarVisitor ) return ((DateGrammarVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_expression);
		try {
			setState(80);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
			case PLUS:
			case MINUS:
				enterOuterAlt(_localctx, 1);
				{
				setState(75);
				operation();
				}
				break;
			case CH:
			case SINCE:
				enterOuterAlt(_localctx, 2);
				{
				setState(76);
				unary();
				}
				break;
			case DATE:
				enterOuterAlt(_localctx, 3);
				{
				setState(77);
				date();
				}
				break;
			case DATETIME:
				enterOuterAlt(_localctx, 4);
				{
				setState(78);
				dateTime();
				}
				break;
			case TIMESPAN:
				enterOuterAlt(_localctx, 5);
				{
				setState(79);
				timeSpan();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3-U\4\2\t\2\4\3\t\3"+
		"\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\5\5\33\n\5\3\5\3\5\5\5\37\n\5\3\5\3\5\5\5#\n\5\3\5\3\5\3\5"+
		"\3\5\3\5\5\5*\n\5\3\5\3\5\3\5\3\5\3\5\5\5\61\n\5\3\5\3\5\3\5\3\5\3\5\5"+
		"\58\n\5\5\5:\n\5\3\5\3\5\3\5\3\5\5\5@\n\5\3\6\3\6\3\6\3\6\5\6F\n\6\3\6"+
		"\3\6\3\6\3\6\5\6L\n\6\3\7\3\7\3\7\3\7\3\7\5\7S\n\7\3\7\2\2\b\2\4\6\b\n"+
		"\f\2\4\3\2,-\3\2*+\2j\2\16\3\2\2\2\4\20\3\2\2\2\6\22\3\2\2\2\b?\3\2\2"+
		"\2\nA\3\2\2\2\fR\3\2\2\2\16\17\7\17\2\2\17\3\3\2\2\2\20\21\7\16\2\2\21"+
		"\5\3\2\2\2\22\23\7\6\2\2\23\7\3\2\2\2\249\t\2\2\2\25\33\5\2\2\2\26\33"+
		"\5\4\3\2\27\33\5\6\4\2\30\33\5\n\6\2\31\33\5\b\5\2\32\25\3\2\2\2\32\26"+
		"\3\2\2\2\32\27\3\2\2\2\32\30\3\2\2\2\32\31\3\2\2\2\33\36\3\2\2\2\34\37"+
		"\5\6\4\2\35\37\5\b\5\2\36\34\3\2\2\2\36\35\3\2\2\2\37:\3\2\2\2 #\5\6\4"+
		"\2!#\5\b\5\2\" \3\2\2\2\"!\3\2\2\2#)\3\2\2\2$*\5\2\2\2%*\5\4\3\2&*\5\6"+
		"\4\2\'*\5\n\6\2(*\5\b\5\2)$\3\2\2\2)%\3\2\2\2)&\3\2\2\2)\'\3\2\2\2)(\3"+
		"\2\2\2*:\3\2\2\2+\61\5\2\2\2,\61\5\4\3\2-\61\5\6\4\2.\61\5\n\6\2/\61\5"+
		"\b\5\2\60+\3\2\2\2\60,\3\2\2\2\60-\3\2\2\2\60.\3\2\2\2\60/\3\2\2\2\61"+
		"\67\3\2\2\2\628\5\2\2\2\638\5\4\3\2\648\5\6\4\2\658\5\n\6\2\668\5\b\5"+
		"\2\67\62\3\2\2\2\67\63\3\2\2\2\67\64\3\2\2\2\67\65\3\2\2\2\67\66\3\2\2"+
		"\28:\3\2\2\29\32\3\2\2\29\"\3\2\2\29\60\3\2\2\2:@\3\2\2\2;<\7\3\2\2<="+
		"\5\b\5\2=>\7\4\2\2>@\3\2\2\2?\24\3\2\2\2?;\3\2\2\2@\t\3\2\2\2AK\t\3\2"+
		"\2BF\5\2\2\2CF\5\4\3\2DF\5\b\5\2EB\3\2\2\2EC\3\2\2\2ED\3\2\2\2FL\3\2\2"+
		"\2GH\7\3\2\2HI\5\b\5\2IJ\7\4\2\2JL\3\2\2\2KE\3\2\2\2KG\3\2\2\2L\13\3\2"+
		"\2\2MS\5\b\5\2NS\5\n\6\2OS\5\2\2\2PS\5\4\3\2QS\5\6\4\2RM\3\2\2\2RN\3\2"+
		"\2\2RO\3\2\2\2RP\3\2\2\2RQ\3\2\2\2S\r\3\2\2\2\r\32\36\")\60\679?EKR";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}